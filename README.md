# vue3-demo

## Vue 3.0 项目初始化

第一步，安装vue-cli：
```
$ npm install -g @vue/cli
```

安装后，测试下是否安装成功：

```
$ vue -V
@vue/cli 4.4.1
```

第二步，初始化 vue 项目：
```
$ vue create vue-next-test
```
输入命令后，会出现命令行交互窗口，和 Vue2.x 的一样，我们选 Manually select features：

```
Vue CLI v4.3.1
? Please pick a preset: 
  default (babel, eslint) 
❯ Manually select features 
```
勾选：Router、Vuex、CSS Pre-processors 和 Linter / Formatter，这些和咱们平常做项目一样：

备注： Vue 3.0 项目目前需要从 Vue 2.0 项目升级而来，所以为了直接升级到 Vue 3.0 全家桶，需要在 Vue 项目创建过程中勾选 Router 和 Vuex。

如果执行 vue create vue-next-test 报错，删除原来的全局配置
```
akryum@akryum-XPS-13-9370:~$ cd .config/yarn/global/
akryum@akryum-XPS-13-9370:~/.config/yarn/global$ rm -rf node_modules/
akryum@akryum-XPS-13-9370:~/.config/yarn/global$ rm yarn.lock 
akryum@akryum-XPS-13-9370:~/.config/yarn/global$ yarn

sudo yarn add global vue // 也执行了
yarn upgrade vue-template-compiler  // 也执行了
```

## 升级项目到 Vue 3.0

```
cd vue-next-test
vue add vue-next
```

## 按以往步骤启动项目
