import { createRouter, createWebHashHistory } from 'vue-router';
import Home from '../views/Home.vue'

const routes = [
{
  path: '/',
  name: 'Home',
  component: Home
},
{
  path: '/getRouter',
  name: 'getRouter',
  // route level code-splitting
  // this generates a separate chunk (about.[hash].js) for this route
  // which is lazy-loaded when the route is visited.
  component: () => import(/* webpackChunkName: "about" */ '../views/GetRouter.vue')
},
{
  path: '/useVuex',
  name: 'UseVuex',
  component: () => import('../views/UseVuex.vue'),
},
{
  path: '/todo',
  name: 'Todo',
  component: () => import('../views/todo/Todo.vue'),
},
{
  path: '/todo1',
  name: 'Todo1',
  component: () => import('../views/todoUnVuex/index.vue'),
},
{
  path: '/cart',
  name: 'Cart',
  component: () => import('../views/cart/index.vue'),
}
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
