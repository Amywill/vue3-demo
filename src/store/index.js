import Vuex from 'vuex'

export default Vuex.createStore({
  state: {
    test: {
      a: 1
    },

    productionList: [
      {
        id: 1,
        title: '商品A',
        price: 10
      },
      {
        id: 2,
        title: '商品B',
        price: 15
      },
      {
        id: 3,
        title: '商品C',
        price: 20
      },
      {
        id: 4,
        title: '商品D',
        price: 100
      },
      {
        id: 5,
        title: '商品E',
        price: 200
      },
      {
        id: 6,
        title: '商品F',
        price: 300
      }
    ],
    cartList: [
      // { id: 1, quantity: 1 }
    ]
  },
  mutations: {
    setTestA(state, value) {
      state.test.a = value
    },
    setProductionList(state, { id, quantity }) {
      state.cartList.push({ id, quantity })
    },
    setCartList(state, { id }) {
      // state.cartList.filter((item) => item.id != id)
      state.cartList.forEach((item, index) => {
        if (item.id === id) {
          state.cartList.splice(index, 1)
        }
      })
    }
  },
  actions: {
  },
  modules: {
  }
});
