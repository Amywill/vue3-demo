const data = {
  name: 'B612',
  age: 6
}

const proxyData  = new Proxy(data, {
  get(target, key, receiver) {
    // 只处理本身（非原型的）属性
    const ownKeys = Reflect.ownKeys(target, key, receiver)
    if (ownKeys.includes(key)) {
      console.log('get', key) // 监听
    }

    const result = Reflect.get(target, key, receiver)
    console.log('get result', result) 
    return result
  },
  set(target, key, val, receiver) {
    // 重复的数据，不处理
    if (val === target[key]) {
      return true
    } 

    const result = Reflect.set(target, key, val, receiver)
    console.log('set', key, val)
    console.log('set result', result) // true
    return result // 是否设置成功
  },
  deleteProperty(target, key) {
    const result = Reflect.deleteProperty(target, key)
    console.log('delete property', key)
    console.log('delete result', result) // true
    return result
  }
})